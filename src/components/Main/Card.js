import React from 'react';
import axios from 'axios';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Collapse } from 'reactstrap';
import Ability from './Ability';

export default class PokeCard extends React.Component {
  state = {
    data: [],
    name: '',
    sprites: '',
    isLoading: true,
    types: [],
    modal: false,
    stats: [],
    collapseStats: false,
  };

  componentDidMount() {
    const { pokemon } = this.props;
    axios.get(pokemon.url)
      .then(res => {
        const pokemonData = res.data;
        this.setState({
          data: pokemonData,
          name: pokemonData.name,
          isLoading: false,
          sprites: pokemonData.sprites,
          types: pokemonData.types,
          stats: pokemonData.stats,
        });
      })
  }

  test = (type) => {
    if(type === 'normal'){
      return '#A8A77A'
    }
    if(type === 'fire') {
      return '#EE8130';
    }
    if(type === 'water'){
      return '#6390F0'
    }
    if(type === 'electric'){
      return '#F7D02C'
    }
    if(type === 'grass'){
      return '#7AC74C'
    }
    if(type === 'ice'){
      return '#96D9D6'
    }
    if(type === 'fighting'){
      return '#C22E28'
    }
    if(type === 'poison'){
      return '#A33EA1'
    }
    if(type === 'ground'){
      return '#E2BF65'
    }
    if(type === 'flying'){
      return '#A98FF3'
    }
    if(type === 'psychic'){
      return '#F95587'
    }
    if(type === 'bug'){
      return '#A6B91A'
    }
    if(type === 'rock'){
      return '#B6A136'
    }
    if(type === 'ghost'){
      return '#735797'
    }
    if(type === 'dragon'){
      return '#6F35FC'
    }
    if(type === 'dark'){
      return '#705746'
    }
    if(type === 'steel'){
      return '#B7B7CE'
    }
    return '#D685AD';
  };

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  };

  toggleCollapseStats = () => {
    const { collapseStats } = this.state;
    this.setState({ collapseStats: !collapseStats });
  };

  render() {
    const { data, name, stats, sprites, types, isLoading } = this.state;
    return(
      <React.Fragment>
        <div className="col-12 col-sm-6 col-md-4 col-lg-3 mt-4">
          {isLoading ? (
            <div className="spinner-border text-primary" role="status">
              <span className="sr-only">Loading...</span>
            </div>
            ) : (
            <React.Fragment>
              <div>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                  <ModalHeader toggle={this.toggle}><small>{`#${data.id} `}</small>{name}</ModalHeader>
                  <ModalBody>
                    <div className="container">
                      {console.log(data)}
                      <div className="d-flex justify-content-between">
                        <img src={sprites.front_default} alt="front default"/>
                        <img src={sprites.back_default} alt="back default"/>
                        <img src={sprites.front_shiny} alt="front shiny"/>
                        <img src={sprites.back_shiny} alt="back shiny"/>
                      </div>
                      <div className="w-100 text-center">
                        <button type="button" className="btn btn-link" onClick={this.toggleCollapseStats}>Stats</button>
                      </div>
                      <Collapse isOpen={this.state.collapseStats}>
                        <label className="mt-3">HP</label>
                        <div className="progress">
                          <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                               aria-valuenow="75" aria-valuemin="0" aria-valuemax="255" style={{ width: `${(stats[5].base_stat)*100/255}%` }}/>
                        </div>
                        <label className="mt-3">Attack</label>
                        <div className="progress">
                          <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                               aria-valuenow="75" aria-valuemin="0" aria-valuemax="255" style={{ width: `${(stats[4].base_stat)*100/190}%` }}/>
                        </div>
                        <label className="mt-3">Defense</label>
                        <div className="progress">
                          <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                               aria-valuenow="75" aria-valuemin="0" aria-valuemax="255" style={{ width: `${(stats[3].base_stat)*100/160}%` }}/>
                        </div>
                        <label className="mt-3">Special Attack</label>
                        <div className="progress">
                          <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                               aria-valuenow="75" aria-valuemin="0" aria-valuemax="255" style={{ width: `${(stats[2].base_stat)*100/154}%` }}/>
                        </div>
                        <label className="mt-3">Special Defense</label>
                        <div className="progress">
                          <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                               aria-valuenow="75" aria-valuemin="0" aria-valuemax="255" style={{ width: `${(stats[1].base_stat)*100/125}%` }}/>
                        </div>
                        <label className="mt-3">Speed</label>
                        <div className="progress mb-3">
                          <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                               aria-valuenow="75" aria-valuemin="0" aria-valuemax="255" style={{ width: `${(stats[0].base_stat)*100/150}%` }}/>
                        </div>
                      </Collapse>
                    </div>
                    <div className="container">
                      <Ability abilities={data.abilities} />
                    </div>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="primary" onClick={this.toggle}>Close</Button>
                  </ModalFooter>
                </Modal>
                </div>
              <div className="card">
                <div className="card-body text-center">
                  <img src={sprites.front_default} alt=""/>
                  <h5 className="card-title"><small>{`#${data.id} `}</small>{name}</h5>
                  <p>
                    {
                      types.map(type => {
                        return <span style={{ backgroundColor: this.test(type.type.name), color: 'white' }} className="badge mx-1">{type.type.name}</span>
                      }).reverse()
                    }
                  </p>
                  <span className="btn btn-custom-primary mt-3" onClick={this.toggle}>Informations</span>
                </div>
              </div>
            </React.Fragment>
          )}
        </div>
      </React.Fragment>
    );
  }
}
