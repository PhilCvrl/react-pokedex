import React from 'react';
import axios from 'axios';
import Card from "./Card";

export default class Pokedex extends React.Component {
  state = {
    pokemonList: [],
    isLoading: true,
  };

  componentDidMount() {
    axios.get(`https://pokeapi.co/api/v2/pokemon/?limit=151&offset=0`)
      .then(res => {
        const pokemonList = res.data;
        this.setState({
          pokemonList,
          isLoading: false,
        });
      })
  }

  render() {
    const { pokemonList, isLoading } = this.state;
    return(
      <div className="row">
        {
          isLoading ? (
            <div className="spinner-border text-primary" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          ) : (
            pokemonList.results.map(pokemon => (
                <Card key={pokemon.url} pokemon={pokemon} />
              )
            )
          )
        }
      </div>
    );
  }
}
