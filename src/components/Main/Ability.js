import React from 'react';
import axios from 'axios';
import { Collapse } from "reactstrap";
import Desc from "./Desc";

class Ability extends React.Component {
  state = {
    collapse: false,
    desc: '',
  };

  toggle = () => {
    const { collapse } = this.state;
    this.setState({
      collapse: !collapse,
    })
  };



  render() {
    const { abilities } = this.props;
    const { collapse } = this.state;
    return(
      <div className="w-100">
        <div className="w-100 text-center">
          <button type="button" className="btn btn-link" onClick={this.toggle}>Abilities</button>
        </div>
        <Collapse isOpen={collapse}>
          <div>
            {abilities.map(ability => {
              return(<p className="my-3">
                  <strong>{`${ability.ability.name} : `}</strong><Desc url={ability.ability.url} />
              </p>);
            })}
          </div>
        </Collapse>
      </div>
    )
  }
}

export default Ability;
