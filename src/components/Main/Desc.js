import React from 'react';
import axios from 'axios';

class Desc extends React.Component {
  state={
    desc: '',
    isLoading: true,
  };

  componentDidMount(){
    const { url } = this.props;
    axios.get(url)
      .then(res => {
        this.setState({
          desc: res.data.effect_entries[0].effect,
          isLoading: false,
        });
      });
  }

  render() {
    const { desc, isLoading } = this.state;
    return(
      <span>
        {isLoading ? ('Loading description...') : (desc)}
      </span>
    )
  }
}

export default Desc;
