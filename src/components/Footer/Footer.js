import React from 'react';

const Footer = () => (
  <footer className="mt-5">
    <div className="container">
      <div className="row align-items-center">
        <div className="col-12">
            <p>Made with love by Tic | BearStudio</p>
        </div>
      </div>
    </div>
  </footer>

);

export default Footer;
